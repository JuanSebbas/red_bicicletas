var map = L.map('main_map').setView([3.3996726,-76.5105985], 130);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', 
{foo: 'bar', attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
L.marker([3.3996726,-76.5105985]).addTo(map);
L.marker([3.3996790,-76.5105285]).addTo(map);
L.marker([3.3996765,-76.5104485]).addTo(map);
